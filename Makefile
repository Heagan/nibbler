NAME = a.out

SRC = 	src/main.cpp \
		src/Location.class.cpp \
		src/Snake.class.cpp \
		src/Game.class.cpp \
		src/SDL.class.cpp \
		src/network.cpp \

INC = -I headers

CC = g++ -std=c++11

SDLLIB		=	$(shell sdl2-config --libs) -lSDL2_net 
SDLFLAGS	=	$(shell sdl2-config --cflags)

ifeq ($(OS), Windows_NT)
        SDLFLAGS	= -I C:/SDL2/net -I C:/SDL2/include
        SDLLIB  	= -L C:/SDL2/lib/x64 -lSDL2main -lSDL2 -L C:/SDL2/net -lSDL2_net
        CC			= x86_64-w64-mingw32-g++ -std=c++11
endif

all:
	@echo "Compiling Project"
	@$(CC) -o $(NAME) $(SRC) $(INC) $(SDLFLAGS) $(SDLLIB)
	@echo "Project Compiled"

clean:
	@echo "Cleaning object files"

fclean: clean
	@echo "Removing binary file" 
	@/bin/rm -f $(NAME)

re: fclean all

#sh -c "$(curl -fsSL https://raw.githubusercontent.com/Tolsadus/42homebrewfix/master/install.sh)"
