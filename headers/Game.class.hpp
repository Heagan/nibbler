#ifndef GAME_H
# define GAME_H

# include "nibbler.h"
# include "Location.class.hpp"
# include "Snake.class.hpp"
# include "SDL.class.hpp"

class Game {

public:
	Game( unsigned int width = SCREEN_WIDTH, unsigned int height = SCREEN_HEIGHT, unsigned int gs = 20 );
	Game( Game &object );
	~Game( void );
	Game &operator=( Game const &object );

	bool		update();
	bool		updateSnakes();

	Snake		&createSnake(int colour, int x = 0, int y = 0);
	void 		createWall(int x, int y);
	void		createBorder( int n = 0 );

	void		respawnApple();

	bool		wallCollision(Loc l);
	bool		appleCollion(Loc l);
	bool		snakeCollion(Snake &snake);
	int			snakeCollionOther(Snake &snake);
	int			snakeCollionSelf(Snake &snake);
	
	void		drawSnake(Snake &snake);
	void		drawWalls( void );
	void		drawFood( void );

	void		refreshBestLength(Snake &snake);

	void		trimTail(Snake &snake);
	void		wrapSnake(Snake &snake);
	void		moveSnake(Snake &snake);
	void		setVelocity(Snake &snake, int xAmount, int yAmount);
	bool		keyPressed();
	bool		networkKeyPressed(std::string message, bool isServer);

	void		printScore( void );

	SDLHandler	*sdl;

	unsigned int width;
	unsigned int height;

	std::vector<Snake>		snakes;
	std::vector<Loc>		walls;

	//grid size / tile size
	int gs;
	int tc;

	//apple
	int ax;
	int ay;
	bool started;
private:

};

#endif
