#ifndef LOCATION_CLASS_H
# define LOCATION_CLASS_H

class Loc {
    public:
        Loc(int x, int y);
        ~Loc();
        int x;
        int y;
};

#endif