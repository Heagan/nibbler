#ifndef SDL_HANDLER_H
# define SDL_HANDLER_H

# include "nibbler.h"

class SDLHandler {

public:
	SDLHandler( int width, int height );
	~SDLHandler( void );
	void	handleInput();
	void	display();
    void	putpixel(unsigned int x, unsigned int y, unsigned int colour) ;
	void	clearpixels();
	void	fill_render(int width, int height, int colour);
	void 	rect(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int colour);
	void	setWinName(std::string name);
	std::vector<eKey>	&getMove() { return pressed; };

	int		width;
	int		height;
	
	SDL_Window			*window;
	SDL_Surface			*surface;
	SDL_Event			event;
	std::vector<eKey>	pressed;

private:

};

#endif
