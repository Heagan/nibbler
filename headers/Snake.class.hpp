#ifndef SNAKE_CLASS_H
# define SNAKE_CLASS_H

# include "nibbler.h"
# include "Location.class.hpp"

class Snake {
public:
    Snake( int colour, int x = 1, int y = 0 );
    ~Snake();
    
    int id;
    //snakes colour
    int colour;

    //Velocity
    double xv;
    double yv;
 
    //Location
    int px;
    int py;

    std::vector<Loc> trail;
    int tail;

    int score;
    int bestTailLength;
};

#endif