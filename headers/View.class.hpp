#ifndef VIEW_H
# define VIEW_H


class View {

protected:
    eKey pressed;
    unsigned int	width;
	unsigned int	height;

public:
	virtual void handleInput() = 0;
	virtual void display() = 0;
    virtual void putpixel(unsigned int x, unsigned int y, unsigned int colour) = 0;
    virtual void rect(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int colour) = 0;
    virtual void clearpixels() = 0;
    virtual void delay(unsigned int n) = 0;
    virtual void destroy() = 0;
    eKey getMove() { return pressed; };
    virtual ~View(){};

};

#endif
