#ifndef NETWORK_H
# define NETWORK_H

# include "nibbler.h"
# include <SDL_net.h>

typedef struct  s_connection {
	UDPpacket   *packet;
	std::string recv_msg;
	void		*special_data;
}               t_con;

class  UDPConnection {

public:
	UDPConnection( );
	~UDPConnection( );

	bool    Init( std::string ip );
	bool    CreatePacket( IPaddress &serverIP );
	void	SetUpServer();
	bool    SetIPAndPort( const std::string &ip, uint16_t port ) ;
	bool    GetIPAndPort( IPaddress &serverIP, const std::string &ip, uint16_t port ) ;
	bool    Send( );
	bool	SendSpecial( void * data, size_t size );
	void    CheckForData();
	bool    WasQuit();
	bool	connect(const std::string &ip, uint16_t port );

	public:
		bool        quit;
		UDPsocket   ourSocket;
		std::string prev_msg;
		std::string ip;
		int32_t remotePort, localPort;

	public:
		std::vector<t_con *>       	connections;
		std::string                 msg;
		bool                        isServer;

};

#endif