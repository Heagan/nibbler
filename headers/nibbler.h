#ifndef NIBBLER_H
#define NIBBLER_H

enum eKey {UP, DOWN, LEFT, RIGHT, QUIT, F1, F2, F3, NUL, UP_PL2, DOWN_PL2, LEFT_PL2, RIGHT_PL2};

#define SCREEN_WIDTH    800
#define SCREEN_HEIGHT   800

#include <string>
#include <iostream>
#include <vector>

#include <math.h>
#include <algorithm>

#include <SDL.h>
#include <ctime>

#include "View.class.hpp"
#include "network.hpp"

int     colour(unsigned char r, unsigned char g, unsigned char b);
std::vector<std::string>	split(const std::string& str, char delim = ',');

#endif
