#ifndef NIBBLER_H
# define NIBBLER_H

# include <string>
# include <iostream>
# include <vector>

# include <math.h>
# include <algorithm>

# include "View.class.hpp"

# define SH 500
# define SW 500

void	clearpixels(View *view);
void    putpixel(View *view, int x, int y, int colour);
void    background(View *view, unsigned char r, unsigned char g, unsigned char b);
void    rect(View *view, int x1, int y1, int xAmount, int yAmount, int colour);

int     colour(unsigned char r, unsigned char g, unsigned char b);

#endif