#include "Game.class.hpp"

Game::Game( unsigned int width, unsigned int height, unsigned int gs ) : width(width), height(height), gs(gs), tc(width / gs) {
	ax = rand() % tc;
	ay = rand() % tc;
	this->started = false;
	this->sdl = new SDLHandler(width, height);
}

Game::Game( Game &object ) {
	*this = object;
}

Game::~Game( void ) {

}

Game	&Game::operator=( Game const &object ) {
	gs = object.gs;
	tc = object.tc;
	ax = object.ax;
	ay = object.ay;
	return *this;
}

void 		Game::createWall(int x, int y) {
	//walls.push_back(*(new Loc(x, y)));
}

Snake	&Game::createSnake(int colour, int x, int y) {
	snakes.push_back(Snake(colour, x, y));
	return snakes.at(snakes.size() - 1);
}

void	Game::moveSnake(Snake &snake) {
		snake.px += snake.xv;
		snake.py += snake.yv;
}

void	Game::wrapSnake(Snake &snake) {
	if (snake.px < 0) {
		snake.px = tc - 1;
	}
	if (snake.px > tc - 1) {
		snake.px = 0;
	}
	if (snake.py < 0) {
		snake.py = tc - 1;
	}
	if (snake.py > tc - 1) {
		snake.py = 0;
	}
}

bool		Game::wallCollision(Loc l) {
	for (int i = 0; i < walls.size(); i++) {
		if ((l.x == walls.at(i).x) && (l.y == walls.at(i).y)) {
			return true;
		}
	}
	return false;
}

bool		Game::snakeCollion(Snake &snake) {
	for (int j = 0; j < snakes.size(); j++) {
		if (wallCollision({snakes.at(j).px, snakes.at(j).py})) {
			printScore();
			return true;
		}
	}
	return false;
}
int			Game::snakeCollionOther(Snake &snake) {
	for (Snake &s : snakes) {
		if (s.id == snake.id) {
			continue ;
		}
		for (int i = snake.trail.size() - 1; i >= 0 ; i--) {
			if (i + 1 == snake.trail.size()) {
				continue ;
			}
			if ((snake.trail.at(i).x == s.px) && (snake.trail.at(i).y == s.py)) {
					printScore();
					return i;
			}
		}
	}
	return 0;

}

int			Game::snakeCollionSelf(Snake &snake) {
	for (int i = snake.trail.size() - 1; i >= 0 ; i--) {
		if (i + 1 == snake.trail.size()) {
			continue ;
		}
		if ((snake.trail.at(i).x == snake.px) && (snake.trail.at(i).y == snake.py)) {
				printScore();
				return i;
		}
	}
	return 0;
}

void		Game::drawWalls( void ) {
	for (int i = 0; i < walls.size(); i++) {
		sdl->rect(walls.at(i).x * gs, walls.at(i).y * gs, walls.at(i).x * gs + gs - 1, walls.at(i).y * gs + gs - 1, 255);
	}
}

void	Game::drawSnake(Snake &snake) {
	for (int i = 0; i < snake.trail.size(); i++) {		
		if (i == 0) {
			sdl->rect(snake.trail[i].x * gs, snake.trail[i].y * gs, snake.trail[i].x * gs + gs - 1, snake.trail[i].y * gs + gs - 1, snake.colour / 3);
		} else if (i + 1 == snake.trail.size()) {
			sdl->rect(snake.trail[i].x * gs, snake.trail[i].y * gs, snake.trail[i].x * gs + gs - 1, snake.trail[i].y * gs + gs - 1, snake.colour / 2);
		} else {
			sdl->rect(snake.trail[i].x * gs, snake.trail[i].y * gs, snake.trail[i].x * gs + gs - 1, snake.trail[i].y * gs + gs - 1, snake.colour);
		}
	}
}

void	Game::trimTail(Snake &snake) {
	snake.trail.push_back(Loc(snake.px, snake.py));
	while (snake.trail.size() > snake.tail) {
		snake.trail.erase(snake.trail.begin());
	}
}

void	Game::respawnApple() {
	do {
		ax = rand() % (tc - 1) ;
		ay = rand() % (tc - 1) ;
	} while (wallCollision({ax, ay}));
}

bool	Game::appleCollion(Loc l) {
	if (ax == l.x && ay == l.y) {
		respawnApple();
		return true;
	}
	return false;
}

void	Game::refreshBestLength(Snake &snake) {
	if (snake.trail.size() > snake.bestTailLength) {
		snake.bestTailLength = snake.trail.size();
	}
}

void		Game::drawFood( void ) {
	sdl->rect(ax * gs, ay * gs, ax * gs + gs - 1, ay * gs + gs - 1, colour(255, 0, 0));
}

void		Game::createBorder( int n ) {
	for (int i = 0; i < tc ; i ++) {
		// if ((i > tc / 9 * 3 ) && (i < tc / 9 * 6)) {
		// 	continue ;
		// }
		createWall(i, n); // TOP
		createWall(i, tc - 1 - n); // BOT
		createWall(n, i); // LEFT
		createWall(tc - 1 - n, i); // RIGHT
	}
}

bool	Game::update() {
	if (wallCollision({ax, ay})) {
		respawnApple();
	}
	updateSnakes();
	drawWalls();
	drawFood();
	return false;
}

bool	Game::updateSnakes() {
	for (Snake &s : snakes) {
		if (appleCollion({s.px, s.py})) {
			s.tail += 1;
			s.score++;
		}
		moveSnake(s);
		wrapSnake(s);
		drawSnake(s);
		trimTail(s);
		refreshBestLength(s);
		s.tail -= snakeCollionOther(s);
		s.tail -= snakeCollionSelf(s);
		if (wallCollision({s.px, s.py})) {
			s.tail = 3;
			s.score -= 10;
			s.px = SCREEN_WIDTH  / 2 / gs;
			s.py = SCREEN_HEIGHT / 2 / gs;
		}
	}
	return false;
}

void		Game::setVelocity(Snake &snake, int xAmount, int yAmount) {
	snake.yv = (snake.yv != -yAmount) ? yAmount : snake.yv;
	snake.xv = (snake.xv != -xAmount) ? xAmount : snake.xv;
}

void		Game::printScore( void ) {
	for (Snake &s : snakes) {
		std::cout << "Player " << s.id << "'s score: " << s.score << std::endl;
	}
}

#include <cmath>

bool		Game::networkKeyPressed(std::string message, bool isServer) {

	bool	snake_exists 				= false;
	std::vector<std::string> strings	= split(message);

	if (strings.size() < 7) {
		return false;
	}
	int		id	 	= atoi(strings[0].c_str());
	int		x	 	= atoi(strings[1].c_str());
	int		y	 	= atoi(strings[2].c_str());
	int		move 	= atoi(strings[3].c_str());
	int		tail 	= atoi(strings[4].c_str());
	int		applex	= atoi(strings[5].c_str());
	int		appley	= atoi(strings[6].c_str());

	if (!isServer) {
		ax = applex;
		ay = appley;
	}
	
	for (Snake &s : snakes) {
		if (s.id == id) {
			snake_exists = true;
			if (!isServer) {
				s.px = x;
				s.py = y;
				s.tail = tail;
			}
			if (move == UP) {
				this->started = not this->started;
				setVelocity(s, 0, -1);
			}
			if (move == DOWN) {
				setVelocity(s, 0, 1);
			}
			if (move == RIGHT) {
				setVelocity(s, 1, 0);
			}
			if (move == LEFT) {
				setVelocity(s, -1, 0);
			}
		}
	}
	
	if (snake_exists == false) {
		Snake &s = createSnake(colour(255, 0, 0), x, y);
		s.id = id;
	}
	if (move == QUIT) {
		printScore();
		return true;
	}
	return false;
}

bool		Game::keyPressed() {
	int keyCode;

	std::vector<eKey>	&pressed = sdl->getMove();
	if (pressed.size() == 0) {
		return false;
	}
	keyCode = pressed[0];
	if (snakes.size() > 0) {
		if (keyCode == UP) {
			setVelocity(snakes.at(0), 0, -1);
			this->started = not this->started;
		}
		if (keyCode == DOWN) {
			setVelocity(snakes.at(0), 0, 1);
		}
		if (keyCode == RIGHT) {
			setVelocity(snakes.at(0), 1, 0);
		}
		if (keyCode == LEFT) {
			setVelocity(snakes.at(0), -1, 0);
		}
		if (keyCode == QUIT) {
			printScore();
			return true;
		}
		if (keyCode == NUL) {
			this->started = false; 
			createSnake(rand() % (255 * 256 * 256 + 255 * 256 + 255), 5, 5);
		}
	}
	pressed.erase(pressed.begin());
	return false;
}