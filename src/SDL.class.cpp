# include "SDL.class.hpp"

SDLHandler::SDLHandler( int width, int height ) {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		throw "Failed to init SDL!";
	}
	//SDL_WINDOWPOS_UNDEFINED
	if (!(this->window = SDL_CreateWindow("Nibber Battle Royal", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
	width, height, SDL_WINDOW_SHOWN))) {
		throw "Failed to create window!";
	}
	if (!(this->surface = SDL_GetWindowSurface(window))) {
		throw "SDL: Failed to get window surface!";
	}
	this->width = width;
	this->height = height;
	pressed.push_back(RIGHT);
}

SDLHandler::~SDLHandler( void ) {
	SDL_FreeSurface(surface);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void	SDLHandler::display( void ) {
	handleInput();
	SDL_UpdateWindowSurface(window);
	clearpixels();
}

void	SDLHandler::putpixel(unsigned int x, unsigned int y, unsigned int colour) {
	unsigned int	*pxlptr;
 	int				lineoffset;

 	if ((int)y >= this->height || (int)y < 0)
 		return;
 	if ((int)x >= this->width || (int)x < 0)
 		return;
	lineoffset = (y * this->width) + x;
	pxlptr = (unsigned int *)surface->pixels;
	pxlptr[lineoffset] = colour;
}

void	SDLHandler::fill_render(int width, int height, int colour) {
	for (int y = 0; y < height ; y++) {
		for (int x = 0; x < width ; x++) {
			putpixel(x, y, colour);
		}
	}
}

void 	SDLHandler::rect(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int colour) {
	for (unsigned int y = y1; y <= y2; y++) {
    	for (unsigned int x = x1; x <= x2; x++) {
			putpixel(x, y, colour);
		}
	}
}

void	SDLHandler::clearpixels( void ) {
	unsigned int *pxlptr;
	pxlptr = (unsigned int *)surface->pixels;
    for (int y = 0; y < this->height; y++) {
        for (int x = 0; x < this->width; x++) {
			pxlptr[((int)y * this->width) + (int)x] = 0;
		}
	}
}


void  	SDLHandler::handleInput( void ) {
	int	keyCode;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT) {
			exit(0);
		}
		if (event.key.state) {
			return ;
		}
		keyCode = event.key.keysym.sym;
		if (keyCode == SDLK_ESCAPE) {
			exit(0);
		}
		if (keyCode == SDLK_UP) {
			pressed.push_back(UP);
		}
		if (keyCode == SDLK_DOWN) {
			pressed.push_back(DOWN);
		}
		if (keyCode == SDLK_LEFT) {
			pressed.push_back(LEFT);
		}
		if (keyCode == SDLK_RIGHT) {
			pressed.push_back(RIGHT);
		}
	}
}

void	SDLHandler::setWinName(std::string name) {
	SDL_SetWindowTitle(window, name.c_str());
}
