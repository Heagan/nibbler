#include "nibbler.h"
#include "Game.class.hpp"
#include "SDL.class.hpp"

const int       FRAMES_PER_SECOND = 15;
Game            *game;
UDPConnection   udpConnection;

int 	colour(unsigned char r, unsigned char g, unsigned char b) {
	return r * 256 * 256 + g * 256 + b;
}

std::vector<std::string>	split(const std::string& str, char delim) {
	std::vector<std::string> strings;
    int		current, previous = 0;

    current = str.find(delim);
    while (current != (int)std::string::npos) {
        strings.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find(delim, previous);
    }
    strings.push_back(str.substr(previous, current - previous));
	return strings;
}

void    sendInput( int i ) {
	std::string s;

	s.append(std::to_string(game->snakes[i].id) + ",");
	s.append(std::to_string(game->snakes[i].px) + ",");
	s.append(std::to_string(game->snakes[i].py) + ",");
	s.append(std::to_string(game->sdl->getMove()[i]) + ",");
	s.append(std::to_string(game->snakes[i].tail) + ",");
	s.append(std::to_string(game->ax) + ",");
	s.append(std::to_string(game->ay) + ",");
	
	if ( udpConnection.WasQuit() ) {
		puts("Connection closed");
		return ;
	}
	udpConnection.msg = s;
	udpConnection.Send();
}

bool    checkNetworkForData() {
	if ( udpConnection.WasQuit() )
		return false;
	udpConnection.CheckForData();
	return true;
}

bool	network() {
	sendInput(0);
	if (udpConnection.isServer) {
		sendInput(1);
	}
	checkNetworkForData();
	for (t_con *t : udpConnection.connections) {
		if (game->networkKeyPressed(t->recv_msg, udpConnection.isServer)) 
			return true;
	}
	return false;
}

void    gameLoop( void ) {
	int frames = 0;
	int borderSize = 0;

	game->sdl->setWinName(udpConnection.isServer ? "Nibbler (Server)" : "Nibbler (Client)");
	game->createBorder();
	game->createSnake(colour(0, 255 ,0), 5, 5);
	while (true) {
		if (game->update()) {
			break ;
		}
		if (game->keyPressed())
			break ;
		game->sdl->display();
		if (frames > 150) {
			borderSize++;
			if (borderSize < SCREEN_WIDTH / game->tc * 3 )
				game->createBorder(borderSize);
			frames = 0;
		}
		frames++;
		if (network())
			break;
		SDL_Delay( ( 1000 / FRAMES_PER_SECOND ) );
	}
}

int     main(int ac, char **av) {
	srand(time(NULL));
	// if (ac < 2) {
	// 	exit(0);
	// }
	udpConnection.Init( "127.0.0.1" );
	game = new Game( SCREEN_WIDTH, SCREEN_HEIGHT, 10 );
	gameLoop();
	return 0;
}

