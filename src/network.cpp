# include "network.hpp"

UDPConnection::UDPConnection( ) {
    quit = false;
    isServer = false;
}

UDPConnection::~UDPConnection( ) {
    for (t_con *t : connections)
        SDLNet_FreePacket(t->packet);
    SDLNet_Quit();
}

// 5555 - is the server
bool UDPConnection::Init( std::string ip ) {
    if ( SDLNet_Init() == -1 ) {
         std::cout << "\tSDLNet_Init failed : " << SDLNet_GetError() << std::endl;
        return false;
    }

    ourSocket = NULL;
    isServer = false;
    std::cout << "Attempting to connect to server port...\n";
    if ((ourSocket = SDLNet_UDP_Open( 5555 )) != NULL) {
        isServer = true;
        std::cout << "Connection successful\nConnection is Server\n";
    } else {
        std::cout << "Connection un-successful\nConnection is Client\n";
    }

    this->ip            = ip;
    this->localPort     = 4444;
    this->remotePort    = 5555;
    if (isServer) {
        localPort  = 5555;
        remotePort = 4444;
    }
	connect(ip, remotePort);

    std::cout << "Connecting to \n\tIP : \t\t" << ip << "\n\tPort : \t\t" << remotePort << std::endl;
    
    if (!ourSocket)
        ourSocket = SDLNet_UDP_Open( localPort );
    return true;
}

bool UDPConnection::CreatePacket( IPaddress &serverIP ) {
    t_con *t = new t_con;

    t->packet = SDLNet_AllocPacket( 512 );
    if (t->packet == NULL) {
        std::cout << "SDLNet_AllocPacket failed : " << SDLNet_GetError() << std::endl;
        return false; 
    }
    t->packet->address.host = serverIP.host; 
    t->packet->address.port = serverIP.port;
    connections.push_back(t);
    std::cout << "Created Packet\n";
    return true;
}

bool UDPConnection::GetIPAndPort( IPaddress &serverIP, const std::string &ip, uint16_t port ) {
    std::cout << "Setting IP ( " << ip << " ) " << "and port ( " << port << " )\n";
    if ( SDLNet_ResolveHost( &serverIP, ip.c_str(), port )  == -1 ) {
        std::cout << "\tSDLNet_ResolveHost failed : " << SDLNet_GetError() << std::endl;
        return false; 
    }
    std::cout << "\tSuccess!\n\n";
    return true; 
}

bool UDPConnection::connect(const std::string &ip, uint16_t port ) {
    IPaddress *serverIP = new IPaddress;
    if (!GetIPAndPort(*serverIP, ip, port)) {
        puts("FALIED TO CREATE IP");
        return false;
    }
    if (!CreatePacket(*serverIP)) {
        puts("FALIED TO CREATE Packet");
        return false;
    }
    return true;
}

void UDPConnection::SetUpServer() {
    for (int i = 0; i < 10; i++) {
        connect(this->ip, this->remotePort + i);
    }
}

bool UDPConnection::Send( ) {
    for (t_con *t : connections) {
        if (msg == prev_msg) {
            return false;
        }
        prev_msg = msg;
        memcpy(t->packet->data, msg.c_str(), msg.length() + 1);
        t->packet->len = msg.length() + 1;
        if ( SDLNet_UDP_Send(ourSocket, -1, t->packet) == 0 ) {
            std::cout << "SDLNet_UDP_Send failed : " << SDLNet_GetError() << "\n";
            return false;
        } else {
           std::cout << "Sent: " << msg << "\n";
        }
    }
    return true;
}

bool UDPConnection::SendSpecial( void * data, size_t size ) {
    for (t_con *t : connections) {
        if (size > t->packet->maxlen) {
            puts("CANT SEND DATA\nToo large");
            std::cout << size << "/" << t->packet->maxlen << std::endl;
            return false;
        }
        memcpy(t->packet->data, data, size);
        t->packet->len = size;
        if ( SDLNet_UDP_Send(ourSocket, -1, t->packet) == 0 ) {
            std::cout << "SDLNet_UDP_Send failed : " << SDLNet_GetError() << "\n";
            return false;
        } else {
            std::cout << "Sent Special data" << "\n";
        }
    }
    return true;
}

void UDPConnection::CheckForData() {
    char        recv_msg[50];
 
    for (t_con *t : connections) {
        if ( SDLNet_UDP_Recv(ourSocket, t->packet)) {
            if (t->packet->len < 50) {
                memset(recv_msg, '\0', 50);
                memcpy(recv_msg, t->packet->data, t->packet->len );
                t->recv_msg = recv_msg;
                std::cout << "Data received: " << t->recv_msg << " " << "\n";
            } else {
                memcpy(t->special_data, t->packet->data, t->packet->len );
                std::cout << "Speacial Data Received\n";
            }
        }
    }
}

bool UDPConnection::WasQuit() {
    return quit;
}
